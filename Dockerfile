FROM openjdk:8u191-jre-alpine

WORKDIR /usr/demo/

ADD baseImages baseImages
ADD compareImages compareImages
ADD image-compare-1.0.jar image-compare-1.0.jar

ENTRYPOINT java -jar image-compare-1.0.jar baseImages/RUN1_060620 compareImages/RUN1_060520